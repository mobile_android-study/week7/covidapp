import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  Widget _BuildCheckbox(int n) {
    return CheckboxListTile(
        value: questionValues[n],
        title: Text(questions[n]),
        onChanged: (newValue) {
          setState(() {
            questionValues[n] = newValue!;
          });
        });
  }

  var questionValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var questions = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อยล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question')),
      body: ListView(
        children: [
          _BuildCheckbox(0),
          _BuildCheckbox(1),
          _BuildCheckbox(2),
          _BuildCheckbox(3),
          _BuildCheckbox(4),
          _BuildCheckbox(5),
          _BuildCheckbox(6),
          _BuildCheckbox(7),
          _BuildCheckbox(8),
          _BuildCheckbox(9),
          _BuildCheckbox(10),
          ElevatedButton(
            onPressed: () async {
              await _saveQuestion();
              Navigator.pop(context);
              },
            child: const Text('Save'))
        ],
      ),
    );
  }
  
  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values')??
    '[false, false, false, false, false, false, false, false, false, false, false]';
    var arrstrQuestionValues = 
    strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i=0; i<arrstrQuestionValues.length; i++) {
        questionValues[i] = (arrstrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionValues.toString());
  }
}
