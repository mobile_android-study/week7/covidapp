import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {
  var vaccine = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadVaccines();
  }

  _loadVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      vaccine = prefs.getStringList('vaccine') ?? ['-', '-', '-'];
    });
  }

  _saveVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccine', vaccine);
    });
  }

  Widget _vaccineDropdownBuilder(
      {required String label,
      required String value,
      ValueChanged<String?>? onChanged}) {
    return Container(
        margin: EdgeInsets.fromLTRB(20, 8, 24, 8),
        child: Row(
          children: [
            Text(label),
            Expanded(
              child: Container(),
            ),
            DropdownButton(
              items: [
                DropdownMenuItem(child: Text('-'), value: '-'),
                DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac'),
                DropdownMenuItem(
                    child: Text('AstraZeneca'), value: 'AstraZeneca'),
                DropdownMenuItem(child: Text('Sinopharm'), value: 'Sinopharm'),
                DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
                DropdownMenuItem(
                    child: Text('Johnson & Johnson'),
                    value: 'Johnson & Johnson'),
                DropdownMenuItem(child: Text('Sputnik V'), value: 'Sputnik V'),
                DropdownMenuItem(child: Text('Novavac'), value: 'Novavac'),
              ],
              value: value,
              onChanged: onChanged,
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Vaccine')),
      body: ListView(
        children: [
          Text('$vaccine'),
          _vaccineDropdownBuilder(
              label: 'First Dose :',
              value: vaccine[0],
              onChanged: (newValue) {
                setState(() {
                  vaccine[0] = newValue!;
                });
              }),
          _vaccineDropdownBuilder(
              label: 'Second Dose :',
              value: vaccine[1],
              onChanged: (newValue) {
                setState(() {
                  vaccine[1] = newValue!;
                });
              }),
          _vaccineDropdownBuilder(
              label: 'Third Dose :',
              value: vaccine[2],
              onChanged: (newValue) {
                setState(() {
                  vaccine[2] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () {
                _saveVaccines();
                Navigator.pop(context);
              },
              child: Text("Save"))
        ],
      ),
    );
  }
}
