import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  Widget _BuildCheckbox(int n) {
    return CheckboxListTile(
        value: RiskValues[n],
        title: Text(Risks[n]),
        onChanged: (newValue) {
          setState(() {
            RiskValues[n] = newValue!;
          });
        });
  }

  var RiskValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var Risks = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Risk')),
      body: ListView(
        children: [
          _BuildCheckbox(0),
          _BuildCheckbox(1),
          _BuildCheckbox(2),
          _BuildCheckbox(3),
          _BuildCheckbox(4),
          _BuildCheckbox(5),
          _BuildCheckbox(6),
          ElevatedButton(
            onPressed: () async {
              await _saveRisk();
              Navigator.pop(context);
              },
            child: const Text('Save'))
        ],
      ),
    );
  }
  
  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_values')??
    '[false, false, false, false, false, false, false]';
    var arrstrRiskValues = 
    strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      for (var i=0; i<arrstrRiskValues.length; i++) {
        RiskValues[i] = (arrstrRiskValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_values', RiskValues.toString());
  }
}
